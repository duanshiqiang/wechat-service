# -*- coding: utf-8 -*-
import webapp2
import hashlib
from model import Session
from xml.dom import minidom
from .handleMsg import handleMsg

class WechatServer(webapp2.RequestHandler):
    def get(self):
        signature = self.request.get('signature')  #微信加密签名
        timestamp = self.request.get('timestamp')  #时间戳
        nonce = self.request.get('nonce')          #随机数
        echostr = self.request.get('echostr')      #随机字符串
        token = "7xfGxH0r6fILnJUMPJyMlX6QGqwooMdI" #pre-defined by me
        
        self.response.headers['Content-Type'] = 'text/plain'
        if( hashlib.sha1("".join( sorted([token, timestamp, nonce]) ) ).hexdigest() == signature):
            self.response.write(echostr)
        else:
            self.response.write("sorry, you are not verified")
            
    def post(self):
        rawData= self.request.body
        try:
            msgXML = minidom.parseString(rawData)
        except:
            #Not XML data
            self.response.write("Sorry")
        else:
            s = Session()
            handle = handleMsg(msgXML, s)
            Session.remove()
            res = handle.response
            self.response.write(res)