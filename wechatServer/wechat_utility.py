# -*- coding: utf-8 -*-
from model import AccessToken, User, Link
import time
from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound
import logging
import requests
import json
import datetime
from model.caching_query import FromCache, RelationshipCache

def getAccessToken(session):
    """This function will return the access token from dabatase,
    if not in db or expired(calculated from the time stored inside the token),
    a new access token will be fetched and updated to db.
    NB: the server may cause the access token expire even the expire time is not
    reached. So also need to check whether it's accepted by server"""
    try:
        token = session.query(AccessToken).options(FromCache("default")).one()
        # I think have 2 seconds bargain is better
        if token.expireTime-2 < int(time.time()):
            newToken = _fetchAccessToken(session, token)
            session.query(AccessToken).options(FromCache("default")).invalidate()
            return session.query(AccessToken).options(FromCache("default")).one()
        else:
            return token
    except MultipleResultsFound as e:
        logging.error(e)
        return None
    except NoResultFound:
        session.query(AccessToken).options(FromCache("default")).invalidate()
        newToken = _fetchAccessToken(session, None)
        return session.query(AccessToken).options(FromCache("default")).one()


def _fetchAccessToken(session, oldToken):
    payload = {'grant_type': 'client_credential', 'secret': '3bf3132a20eba0601d0a217d45cd3ead', 'appid': 'wx9f0493b628bda9bd'}
    response = requests.get('https://api.weixin.qq.com/cgi-bin/token', params=payload).json()
    try:
        tokenString = response['access_token']
        expires_in = response['expires_in']
        expireTime = int(time.time()) + expires_in
        if not oldToken:
            newToken = AccessToken(token=tokenString, expireTime=expireTime)
            session.add(newToken)
            try:
                session.commit()
            except:
                session.rollback()
                #raise
            return newToken
        else:
            oldToken.token = tokenString
            oldToken.expireTime = expireTime
            # here the session will automatically notice the change
            try:
                session.commit()
            except:
                session.rollback()
                #raise
            return oldToken
    except KeyError:
        logging.error("Error fetch access token from wechat server: " + str(response))
        return None


def refreshSubscribedUser(session):
    """Refresh the users in database"""
    #获取关注者列表
    url = 'https://api.weixin.qq.com/cgi-bin/user/get'
    token = getAccessToken(session).token
    payload = {'access_token': token, 'next_openid': ''}
    response = requests.get(url, params=payload).json()
    try:
        openIDs = response['data']['openid']
        # Get all user info from db
        recordedUsers = session.query(User).options(FromCache("default")).all()
        leftRecordedUsers = recordedUsers[:]
        # url for 获取用户基本信息
        urlUser = 'https://api.weixin.qq.com/cgi-bin/user/info'
        currentUsers_Json = []
        for openID in openIDs:
            payloadUser = {'access_token': token, 'openid': openID}
            reqUser = requests.get(urlUser, params=payloadUser)
            # Here should not throw KeyError if wechat server is working without error
            currentUsers_Json.append(reqUser.json())
        # There are three kinds of users to handle 
        # 1. already existed in db
        # 2. not in db
        # 3. in db but have de-subscribed
        for currentUser_Json in currentUsers_Json:
            if currentUser_Json['openid'] in [user.openID for user in recordedUsers]:
                for recordedUser in recordedUsers:
                    if currentUser_Json['openid'] == recordedUser.openID:
                        if not recordedUser.subscribed:
                            recordedUser.subscribed = True
                        if currentUser_Json['nickname'] != recordedUser.nickname:
                            recordedUser.nickname = currentUser_Json['nickname']
                        if currentUser_Json['headimgurl'] != recordedUser.headimgurl:
                            recordedUser.headimgurl = currentUser_Json['headimgurl']
                        leftRecordedUsers.remove(recordedUser)
                        break
            else:
                newUser = User(currentUser_Json['openid'], currentUser_Json['nickname'], \
                               currentUser_Json['headimgurl'], True)
                session.add(newUser)
        for user in leftRecordedUsers:
            user.subscribed = False
        
        if len(session.dirty) or len(session.new):
            try:
                session.commit()
            except:
                session.rollback()
                #raise
            session.query(User).options(FromCache("default")).invalidate()
        
        return bool(session.query(User).options(FromCache("default")).all())
            
    except KeyError:
        logging.error("Error 获取关注者列表: " + str(response))
        return None


def createUIMenu(session):
    token = getAccessToken(session).token
    url = 'https://api.weixin.qq.com/cgi-bin/menu/create'
    payload = {'access_token': token}
    
    menu = {}
    l1B0 = {'name': u'聊天', 'sub_button': []}
    l2B0_0 = {'type': 'click', 'name': u'我在和谁聊?', 'key': 'CheckChatPartner'}
    l2B0_1 = {'type': 'click', 'name': u'绑定聊天伙伴', 'key': 'HelpMeBindAPartner'}
    l2B0_2 = {'type': 'click', 'name': u'用户列表', 'key': 'SeeUserList'}
    l2B0_3 = {'type': 'click', 'name': u'结束聊天绑定', 'key': 'CancelBinding'}
    l1B0['sub_button'].extend([l2B0_0, l2B0_1, l2B0_2, l2B0_3])
    menu['button'] = [l1B0,]
    
    req = requests.post(url, params=payload, data=json.dumps(menu, ensure_ascii=False).encode('utf-8'))
    if req.json()['errcode'] != 0:
        logging.error("Error creating UI menu: " + req.text)
        return False
    else:
        return True

def chatBind(session, A, B):
    """Bind two wechat user together so that they can chat through this channel
    A is the "Link Creator", B is the user A want to bind"""
    link = _queryLink(session, A)
    if link:
        if link.confirmed:
            linkusers = [link.created_User, link.accepted_User]
            linkusers.remove(A)
            linkpartner = linkusers[0]
            if linkpartner == B:
                return u"您已经和 " + B.nickname + u" 绑定, 无需重新绑定"
            else:
                return u"您现在和 " + linkpartner.nickname + u" 绑定, 请先取消绑定再绑定其他用户"
        else:
            if link.created_User == A and link.accepted_User == B:
                return u"您已经请求和 " + B.nickname + u" 绑定, 对方同意后即可进行聊天"
            elif link.created_User == A and link.accepted_User != B:
                # need to change old link
                link.accepted_User = B
                
                try:
                    session.commit()
                except:
                    session.rollback()
                    #raise
                session.query(Link).options(FromCache("default")).invalidate()
                session.query(Link).options(FromCache("default")).all()
                
                return u"您已经请求和 " + B.nickname + u" 绑定, 对方同意后即可进行聊天"
            elif link.accepted_User == A and link.created_User == B:
                # This is accepting link request
                link.confirmed = True
                
                try:
                    session.commit()
                except:
                    session.rollback()
                    #raise
                session.query(Link).options(FromCache("default")).invalidate()
                session.query(Link).options(FromCache("default")).all()
                
                sendresult = sendMsgActively(session, B, A.nickname + u" 已经接受您的绑定, 开始聊天吧!")
                if sendresult:
                    return u"您已经和 " + B.nickname + u" 绑定, 开始聊天吧!"
                else:
                    return u"您已经和 " + B.nickname \
                        + u" 绑定, 但无法通知对方(可能对方已经24小时没有和此公众号码互动), 您可以单独联系对方互动一次"
            elif link.accepted_User == A and link.created_User != B:
                session.delete(link)
                newlink = Link(A, B, datetime.datetime.utcnow(), False)
                session.add(newlink)
                
                try:
                    session.commit()
                except:
                    session.rollback()
                    #raise
                session.query(Link).options(FromCache("default")).invalidate()
                session.query(Link).options(FromCache("default")).all()
                
                sendMsgActively(session, link.created_User, A.nickname + u"已经拒绝您的绑定申请.")
                return u"您已经请求和 " + B.nickname + u" 绑定, 对方同意后即可进行聊天"
    else:
        newlink = Link(A, B, datetime.datetime.utcnow(), False)
        session.add(newlink)
        
        try:
            session.commit()
        except:
            session.rollback()
            #raise
        session.query(Link).options(FromCache("default")).invalidate()
        session.query(Link).options(FromCache("default")).all()
                
        sendMsgActively(session, B, A.nickname + u"申请和您绑定聊天, 回复[bind %s]确认" % A.nickname)
        return u"您已经请求和 " + B.nickname + u" 绑定, 对方同意后即可进行聊天"


def deleteLink(session, user):
    link = _queryLink(session, user)
    if link:
        session.delete(link)
        try:
            session.commit()
        except:
            session.rollback()
            #raise
        session.query(Link).options(FromCache("default")).invalidate()
        session.query(Link).options(FromCache("default")).all()
        return True
    else:
        return False
        
def _queryLink(session, A):
    """Query whether A is in a link(whether as creator or acceptor).
    Whether the link is established or not.
    the link will be returned if A is in that link, 
    return None else"""
    allLinks = session.query(Link).options(FromCache("default")) \
       .options((RelationshipCache(Link.created_User, 'default')).and_ \
       (RelationshipCache(Link.accepted_User, 'default'))).all()
    for link in allLinks:
        if link.created_User == A:
            return link
        if link.accepted_User == A:
            return link
    return None


def queryEstablishedLink(session, user):
    link = _queryLink(session, user)
    if link and link.confirmed: 
        return link
    else:
        return None


def getLinkPartner(session, user):
    """will return None if not bound with other user"""
    link = queryEstablishedLink(session, user)
    if link:
        linkusers = [link.created_User, link.accepted_User]
        linkusers.remove(user)
        linkpartner = linkusers[0]
        return linkpartner
    else:
        return None


def sendMsgActively(session, user, content, msgType='text'):
    """Send so-called 客服消息(shit!) to individual user, msgType can be text or image"""
    url = 'https://api.weixin.qq.com/cgi-bin/message/custom/send'
    token = getAccessToken(session).token
    payload = {'access_token': token}
    if msgType=='text':
        data = {'touser': user.openID, 'msgtype': 'text', 'text': {'content': content}}
    elif msgType=='image':
        data = {'touser': user.openID, 'msgtype': 'image', 'image': {'media_id': content}}
    elif msgType=="voice":
        data = {'touser': user.openID, 'msgtype': 'voice', 'voice': {'media_id': content}}
    elif msgType=="video":
        data = {'touser': user.openID, 'msgtype': 'video', 'video': {'media_id': content}}
    else:
        return False
    req = requests.post(url, params=payload, data=json.dumps(data, ensure_ascii=False).encode('utf-8'))
    response = req.json()
    if response['errcode'] != 0:
        logging.error("Error sending text msg actively: " + req.text)
        return False
    else:
        return True


def getUserByOpenID(session, openID, forceRefresh=False):
    if forceRefresh:
        refreshSubscribedUser(session)
    users = session.query(User).options(FromCache('default')).all()
    for user in users:
        if user.openID == openID:
            return user
    return None


def getUserByNickname(session, nickname, forceRefresh=False):
    """query user by nickname(NB:there may be more than 1 user having same nickname)
    this fuction only return the first one"""
    if forceRefresh:
        refreshSubscribedUser(session)
    users = session.query(User).options(FromCache('default')).all()
    for user in users:
        if user.nickname == nickname:
            return user
    return None


def getAvailableUserList(session, forceRefresh=False):
    if forceRefresh:
        refreshSubscribedUser(session)
    users = session.query(User).options(FromCache("default")).all()
    # Then remove unsubscibed users
    users[:] = [user for user in users if user.subscribed]
    users[:] = [user for user in users if not (_queryLink(session, user) and _queryLink(session, user).confirmed)]
    return users