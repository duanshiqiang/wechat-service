# -*- coding: utf8 -*-
import logging
import re
import time

from model import Message

from .wechat_utility import *


textResTemplate = u"""<xml>
<ToUserName><![CDATA[%s]]></ToUserName>
<FromUserName><![CDATA[%s]]></FromUserName>
<CreateTime>%s</CreateTime>
<MsgType><![CDATA[%s]]></MsgType>
<Content><![CDATA[%s]]></Content>
</xml>"""

WECHATACCOUT = "gh_b773ad19ae24"

class handleMsg():
    
    def __init__(self, Object, session):
        """Object should be a xml.dom.minidom.Document instance"""
        self.wechatXML = Object
        self.session = session
        self.__MsgType = wechatXMLNodeData(self.wechatXML, "MsgType")
        self.__FromUserName = wechatXMLNodeData(self.wechatXML, "FromUserName")
        self.__ToUserName = wechatXMLNodeData(self.wechatXML, "ToUserName")
        self.__CreateTime = wechatXMLNodeData(self.wechatXML, "CreateTime")
        self.response = None
        logging.info("received msg from wechat follwer: " \
                     + self.__FromUserName)
        if self.__ToUserName !=  WECHATACCOUT:
            self.response = ""
        else:
            getattr(self, "handle"+self.__MsgType)()
    
    def handletext(self):
        """Hanlde wechat txt message"""
        content = wechatXMLNodeData(self.wechatXML, "Content")
        #check whether it's for sending sms
        reg_bind = re.compile(r'[Bb][Ii][Nn][Dd]\s(.*)$', re.U)
        
        if reg_bind.match(content):
            bindnickname = reg_bind.findall(content)[0]
            A = getUserByOpenID(self.session, self.__FromUserName)
            B = getUserByNickname(self.session, bindnickname, True)
            if B:
                self.response = textResTemplate % (self.__FromUserName, WECHATACCOUT,
                                                    str(int(round(time.time()))),
                                                    "text", chatBind(self.session, A, B))
            else:                    
                self.response = textResTemplate % (self.__FromUserName, WECHATACCOUT, 
                                                            str(int(round(time.time()))),
                                                            "text", u"抱歉, 没有找到用户"+bindnickname)
        else:
            user = getUserByOpenID(self.session, self.__FromUserName)
            linkpartner = getLinkPartner(self.session, user)
            if linkpartner:
                sendresult = sendMsgActively(self.session, linkpartner, content)
                msg = Message(user, linkpartner, datetime.datetime.utcnow(), content)
                self.session.add(msg)
                try:
                    self.session.commit()
                except:
                    self.session.rollback()
                    #raise
                if sendresult:
                    self.response = ""
                else:
                    self.response = textResTemplate % (self.__FromUserName, WECHATACCOUT, 
                                                       str(int(round(time.time()))),
                                                       "text", u"抱歉, 发送失败")
            else:
                self.response = textResTemplate % (self.__FromUserName, WECHATACCOUT, 
                                                   str(int(round(time.time()))),
                                                   "text", "Message received")
    
    def handleimage(self):
        picUrl = wechatXMLNodeData(self.wechatXML, 'PicUrl')
        mediaId =  wechatXMLNodeData(self.wechatXML, 'MediaId')
        user = getUserByOpenID(self.session, self.__FromUserName)
        linkpartner = getLinkPartner(self.session, user)
        if linkpartner:
            sendresult = sendMsgActively(self.session, linkpartner, mediaId, 'image')
            msg = Message(user, linkpartner, datetime.datetime.utcnow(), picUrl)
            self.session.add(msg)
            try:
                self.session.commit()
            except:
                self.session.rollback()
                #raise
            if sendresult:
                self.response = ""
            else:
                self.response = textResTemplate % (self.__FromUserName, WECHATACCOUT, 
                                                   str(int(round(time.time()))),
                                                   "text", u"抱歉, 发送失败")
        else:
            self.response = textResTemplate % (self.__FromUserName, WECHATACCOUT, 
                                                str(int(round(time.time()))),
                                                "text", "Message received")
        
    def handlevoice(self):
        mediaId =  wechatXMLNodeData(self.wechatXML, 'MediaId')
        user = getUserByOpenID(self.session, self.__FromUserName)
        linkpartner = getLinkPartner(self.session, user)
        if linkpartner:
            sendresult = sendMsgActively(self.session, linkpartner, mediaId, 'voice')
            msg = Message(user, linkpartner, datetime.datetime.utcnow(), mediaId)
            self.session.add(msg)
            try:
                self.session.commit()
            except:
                self.session.rollback()
                #raise
            if sendresult:
                self.response = ""
            else:
                self.response = textResTemplate % (self.__FromUserName, WECHATACCOUT, 
                                                   str(int(round(time.time()))),
                                                   "text", u"抱歉, 发送失败")
        else:
            self.response = textResTemplate % (self.__FromUserName, WECHATACCOUT, 
                                                str(int(round(time.time()))),
                                                "text", "Message received")
    def handlevideo(self):
        mediaId =  wechatXMLNodeData(self.wechatXML, 'MediaId')
        user = getUserByOpenID(self.session, self.__FromUserName)
        linkpartner = getLinkPartner(self.session, user)
        if linkpartner:
            sendresult = sendMsgActively(self.session, linkpartner, mediaId, 'voice')
            msg = Message(user, linkpartner, datetime.datetime.utcnow(), mediaId)
            self.session.add(msg)
            try:
                self.session.commit()
            except:
                self.session.rollback()
                #raise
            if sendresult:
                self.response = ""
            else:
                self.response = textResTemplate % (self.__FromUserName, WECHATACCOUT, 
                                                   str(int(round(time.time()))),
                                                   "text", u"抱歉, 发送失败")
        else:
            self.response = textResTemplate % (self.__FromUserName, WECHATACCOUT, 
                                                str(int(round(time.time()))),
                                                "text", "Message received")
        
    def handleevent(self):
        eventType = wechatXMLNodeData(self.wechatXML, 'Event')
        if eventType == 'CLICK':
            eventKey = wechatXMLNodeData(self.wechatXML, 'EventKey')
            if eventKey == 'CheckChatPartner':
                user = getUserByOpenID(self.session, self.__FromUserName)
                linkpartner = getLinkPartner(self.session, user)
                if linkpartner:
                    self.response = textResTemplate % (self.__FromUserName, WECHATACCOUT, 
                                                       str(int(round(time.time()))),
                                                       "text", u"您正在和" + linkpartner.nickname + u"绑定")
                else:
                    self.response = textResTemplate % (self.__FromUserName, WECHATACCOUT, 
                                                       str(int(round(time.time()))),
                                                       "text", u"您未和任何人绑定")
                    
            elif eventKey == 'HelpMeBindAPartner':
                self.response = textResTemplate % (self.__FromUserName, WECHATACCOUT, 
                                                   str(int(round(time.time()))),
                                                   "text", u"请发送[bind 对方昵称]来申请和其他用户绑定")
            elif eventKey == 'SeeUserList':
                self.response = textResTemplate % (self.__FromUserName, WECHATACCOUT, 
                                                   str(int(round(time.time()))),
                                                   "text", u"\n".join([user.nickname for user in getAvailableUserList(self.session)]))
            elif eventKey == 'CancelBinding':
                user = getUserByOpenID(self.session, self.__FromUserName)
                if deleteLink(self.session, user):
                    self.response = textResTemplate % (self.__FromUserName, WECHATACCOUT, 
                                                       str(int(round(time.time()))),
                                                       "text", u"成功取消绑定")
                else:
                    self.response = textResTemplate % (self.__FromUserName, WECHATACCOUT, 
                                                       str(int(round(time.time()))),
                                                       "text", u"抱歉, 您未和任何用户绑定")
            else:
                self.response = textResTemplate % (self.__FromUserName, WECHATACCOUT, 
                                                                str(int(round(time.time()))),
                                                                "text", u"Message received")
        elif eventType == 'subscribe' or 'unsubscribe':
            """Just refresth the subscribe list should be okay(I think)"""
            refreshSubscribedUser(self.session)
            self.response = ''
        else:
            self.response = ''
        

def wechatXMLNodeData(Object, nodeName):
    """ valueType should be "String", "CDATA", "Number".
    Object should be the xml.dom.minidom.Document instance.
    Always return unicode String"""
    try:
        return Object.getElementsByTagName(nodeName)[0].firstChild.data
    except:
        return ""