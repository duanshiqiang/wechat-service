# -*- coding: utf-8 -*-
import webapp2
from model import Session
from .chatqueryJson import messages_Json
import jinja2

jinja_env = jinja2.Environment(autoescape=False,
    loader=jinja2.PackageLoader('chatweb', 'templates'))

class wechatLogWeb(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/html'
        self.response.write('<html><head><meta property="qc:admins" content="6601305623014477645163011116763757" /> \
            </head><body></body></html>')
    
    def post(self):
        self.resposne.write('')


class wechatLogWebJson(webapp2.RequestHandler):
    def get(self):
        user1nickname = self.request.get('user1')
        user2nickname = self.request.get('user2')
        offset = self.request.get('offset', default_value=0)
        limit = self.request.get('limit', default_value=1000)
        s = Session()
        resjson = messages_Json(s, user1nickname, user2nickname, offset, limit)
        s.close()
        Session.remove()
        self.response.write(resjson)