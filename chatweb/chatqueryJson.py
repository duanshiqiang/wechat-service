# -*- coding: utf-8 -*-
from model import Message
from sqlalchemy import or_, and_
from model.caching_query import FromCache, RelationshipCache
from wechatServer.wechat_utility import *
import json

def messages_Json(session, user1nickname, user2nickname, offset, limit):
    output = {}
    user1 = getUserByNickname(session, user1nickname)
    user2 = getUserByNickname(session, user2nickname)
    msgQuery = session.query(Message).options(FromCache('shortlived')) \
        .options((RelationshipCache(Message.send_User, 'shortlived')).and_ \
                 (RelationshipCache(Message.received_User, 'shortlived'))) \
        .filter(or_(and_(Message.send_User==user1, Message.received_User==user2), \
                    and_(Message.send_User==user2, Message.received_User==user1))) \
        .offset(offset).limit(limit)
    messages = msgQuery.all()
    output['users'] = [user1.nickname, user2.nickname]
    output['count'] = len(messages)
    output['offset'] = offset
    output['messages'] = [{'send': i.send_User.nickname, 'receive': i.received_User.nickname, 'sendTime': i.sendTime.isoformat(), 'content': i.content} for i in messages]
    return json.dumps(output, indent=4, ensure_ascii=False)