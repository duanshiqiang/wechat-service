# -*- coding: utf-8 -*-
"""environment.py

Establish data / cache file paths, and configurations,
bootstrap fixture data if necessary.

"""
from . import caching_query
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from dogpile.cache.region import make_region
import os
from hashlib import md5
import sys
import types
py2k = sys.version_info < (3, 0)

if py2k:
    input = raw_input

# dogpile cache regions.  A home base for cache configurations.
regions = {}


# scoped_session.  Apply our custom CachingQuery class to it,
# using a callable that will associate the dictionary
# of regions with the Query.
Session = scoped_session(
                sessionmaker(
                    query_cls=caching_query.query_callable(regions)
                )
            )

# global declarative base class.
Base = declarative_base()

engine = create_engine('mysql+mysqldb://wechat:lfdJwB5zvV2u@localhost:3306/wechat?charset=utf8&use_unicode=0', echo=False, pool_recycle=300)
Session.configure(bind=engine)


def md5_key_mangler(key):
    """Receive cache keys as long concatenated strings;
    distill them into an md5 hash.

    """
    if type(key) == types.UnicodeType:
        key = key.encode('utf-8')
    return md5(key).hexdigest()

# configure the "default" cache region.
regions['default'] = make_region(
            # the "dbm" backend needs
            # string-encoded keys
            key_mangler=md5_key_mangler
        ).configure(
        'dogpile.cache.pylibmc',
        expiration_time = 7200,
        arguments = {'url':["127.0.0.1"],}
    )

regions['shortlived'] = make_region(
            key_mangler=md5_key_mangler
        ).configure(
        'dogpile.cache.pylibmc',
        expiration_time = 60,
        arguments = {'url':["127.0.0.1"],}
    )
        
# optional; call invalidate() on the region
# once created so that all data is fresh when
# the app is restarted.  Good for development,
# on a production system needs to be used carefully
# regions['default'].invalidate()