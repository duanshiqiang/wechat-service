# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, String, Sequence, ForeignKey, Boolean, DateTime
from sqlalchemy.orm import relationship, backref
from .caching_query import FromCache, RelationshipCache
from .environment import Base, engine

class AccessToken(Base):
    __tablename__ = 'accessToken'
    id = Column(Integer, Sequence('accessToken_id_seq'), primary_key=True)
    token = Column(String(200))
    expireTime = Column(Integer)


class User(Base):
    __tablename__ = 'user'
    
    id = Column(Integer, Sequence('user_id_seq'), primary_key=True)
    openID = Column(String(100), nullable=False)
    nickname = Column(String(50, convert_unicode=True))
    headimgurl = Column(String(200))
    subscribed = Column(Boolean)
    
    def __init__(self, openID, nickname, headimgurl, subscribed):
        self.openID = openID
        self.nickname = nickname
        self.headimgurl = headimgurl
        self.subscribed = subscribed
        
    def __repr__(self):
        return "<User(nickname='%s')>" % self.nickname.encode('utf-8')


class Message(Base):
    __tablename__ = 'message'
    
    id = Column(Integer, Sequence('message_id_seq'), primary_key=True)
    send_User_id = Column(Integer, ForeignKey('user.id'))
    received_User_id = Column(Integer, ForeignKey('user.id'))
    sendTime = Column(DateTime)
    content = Column(String(500, convert_unicode=True))
    
    send_User = relationship('User', foreign_keys=[send_User_id], backref='sentMsg')
    received_User = relationship('User', foreign_keys=[received_User_id], backref='receivedMsg')
    
    def __init__(self, send_User, received_User, sendTime, content):
        self.send_User = send_User
        self.received_User = received_User
        self.sendTime = sendTime
        self.content = content
    
    def __repr__(self):
        return "<Message sent by %s to %s at %s>" % \
            (self.send_User.nickname.encode('utf-8'), self.received_User.nickname.encode('utf-8'), self.sendTime.isoformat()) 


class Link(Base):
    __tablename__ = 'link'
    
    id = Column(Integer, Sequence('link_id_seq'), primary_key=True)
    created_User_id = Column(Integer, ForeignKey('user.id'))
    accepted_User_id = Column(Integer, ForeignKey('user.id'))
    created_Time = Column(DateTime)
    confirmed = Column(Boolean)
    
    created_User = relationship('User', foreign_keys=[created_User_id], backref='createdLink', uselist=False)
    accepted_User = relationship('User', foreign_keys=[accepted_User_id], backref='acceptedLink', uselist=False)
    
    def __init__(self, created_User, accepted_User, created_Time, confirmed):
        self.created_User = created_User
        self.accepted_User = accepted_User
        self.created_Time = created_Time
        self.confirmed = confirmed


def createSchema():
    Base.metadata.create_all(engine)