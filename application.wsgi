# -*- coding: utf-8 -*-
import sys
import os
import logging

path = os.path.dirname(os.path.realpath(__file__))
if path not in sys.path:
    sys.path.insert(0, path)
virtualenvActivatePath = "/home/ubuntu/venv/wechat/bin/activate_this.py"
execfile(virtualenvActivatePath, dict(__file__=virtualenvActivatePath))

import webapp2
from chatweb.app import wechatLogWeb
from wechatServer.app import WechatServer

wechatServerApp = webapp2.WSGIApplication([('/', WechatServer),], debug=False)
#wechatLogWebApp = webapp2.WSGIApplication([('/', wechatLogWeb), ('/.json', wechatLogWebJson)], debug=True)
wechatLogWebApp = webapp2.WSGIApplication([('/', wechatLogWeb)], debug=True)